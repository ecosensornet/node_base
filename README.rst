========================
node_base
========================

.. {# pkglts, doc

.. image:: https://ecosensornet.gitlab.io/node_base/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/node_base/0.0.1/

.. image:: https://ecosensornet.gitlab.io/node_base/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/node_base

.. image:: https://ecosensornet.gitlab.io/node_base/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://ecosensornet.gitlab.io/node_base/

.. image:: https://badge.fury.io/py/node_base.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/node_base

.. #}
.. {# pkglts, glabpkg_dev, after doc

.. #}

Base template for all nodes in the network

