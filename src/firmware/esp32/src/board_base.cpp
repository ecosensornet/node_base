#include <Arduino.h>
#include <LittleFS.h>

#include "settings.h"

#include "board_base.h"

unsigned long last_blip;
unsigned long led_delay;
unsigned long warning_delay;


void board::setup_base() {
  pinMode(LED_SIGNAL, OUTPUT);
  last_blip = millis();
  digitalWrite(LED_SIGNAL, LOW);
  led_delay = 1000;
  warning_delay = 100;

#if (DEBUG == 1)
  Serial.begin(115200);
  while (!Serial) {
    delay(10);
    board::signal_warning(0);
  }
  delay(300);
  Serial.print("started in :");
  Serial.print(millis());
  Serial.println(" [ms]");
  delay(1500);
#endif  // DEBUG

  // config and calibration
  LittleFS.begin();

}


void board::signal_normal(){
  if (millis() - last_blip > led_delay) {
    if (led_delay == 1000) {
      digitalWrite(LED_SIGNAL, HIGH);
      led_delay = 100;
    } else {
      digitalWrite(LED_SIGNAL, LOW);
      led_delay = 1000;
    }
    last_blip = millis();
  }
}


void board::signal_warning(uint8_t lvl){
  if (millis() - last_blip > warning_delay) {
    if (warning_delay == 100) {
      digitalWrite(LED_SIGNAL, HIGH);
      warning_delay = 99;
    } else {
      digitalWrite(LED_SIGNAL, LOW);
      warning_delay = 100;
    }
    last_blip = millis();
  }
}
