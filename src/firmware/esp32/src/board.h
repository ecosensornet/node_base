#pragma once

#include "board_base.h"
#include "settings.h"

// measure
//#define PIN_TH_3V3 39
//#define PIN_TH_DATA 3

// LORA
#define PIN_SPI_SCLK 7  // fixed since SPI.begin() smw in lmic code
#define PIN_SPI_MISO 9  // fixed since SPI.begin() smw in lmic code
#define PIN_SPI_MOSI 11  // fixed since SPI.begin() smw in lmic code
#define PIN_LORA_CS 12
#define PIN_LORA_RST 5
#define PIN_LORA_DIO0 16
#define PIN_LORA_DIO1 18
#define PIN_LORA_DIO2 33
