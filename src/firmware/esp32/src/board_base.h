#pragma once

#define LED_SIGNAL 15

namespace board {

void setup_base();
void setup();
void signal_normal();
void signal_warning(uint8_t);

}  // namespace board
