#include <Arduino.h>

#include "../board.h"
#include "../settings.h"
#include "../calibration/calibration.h"

#include "measure.h"

measure::Sample sample_cur;

void measure::setup() {}


void measure::sample(){
    uint32_t s1 = 123;
    sample_cur.sensor1 = s1;
    sample_cur.sensor1_cal = calib.sensor1[0] + calib.sensor1[1] * s1;

    uint32_t s2 = 456;
    sample_cur.sensor2 = s2;
    sample_cur.sensor2_cal = calib.sensor2[0] + calib.sensor2[1] * s2 + calib.sensor2[2] * s2 * s2;
}
