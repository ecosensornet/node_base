#pragma once

#include <Arduino.h>

#include "measure_base.h"

namespace measure {

typedef struct {
  uint32_t sensor1;
  float sensor1_cal;
  uint32_t sensor2;
  float sensor2_cal;
} Sample;

}  // namespace measure

extern measure::Sample sample_cur;
