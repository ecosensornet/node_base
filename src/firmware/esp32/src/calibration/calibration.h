#pragma once

#include "calibration_base.h"


namespace calibration {

typedef struct {
  float sensor1[2];
  float sensor2[3];
} Calibration;

}  // namespace calibration

extern calibration::Calibration calib;
