#include <Arduino.h>

#include "../calibration/calibration.h"
#include "../config/config.h"

#include "html_calibration.h"
#include "mode_config.h"


String html_calibration::processor(const String& key) {
  if (key == "val_cur") {
    return meas_cur;
  }
  else if (key == "nid") {
    return cfg.node_id;
  }
  else if (key == "sensor1_coeff0") {
    return String(calib.sensor1[0]);
  }
  else if (key == "sensor1_coeff1") {
    return String(calib.sensor1[1]);
  }
  else if (key == "sensor2_coeff0") {
    return String(calib.sensor2[0]);
  }
  else if (key == "sensor2_coeff1") {
    return String(calib.sensor2[1]);
  }
  else if (key == "sensor2_coeff2") {
    return String(calib.sensor2[2]);
  }

  return "Key not found";
}

void html_calibration::handle_POST() {
#if (DEBUG == 1)
    Serial.println("POST to calibration.html");
    String message = "POST form was:\n";
    for (uint8_t i = 0; i < server.args(); i++) {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    Serial.println(message);
#endif

    for (uint8_t i = 0; i < server.args(); i++) {
      if (server.argName(i) == "sensor1_coeff0") {
        calib.sensor1[0] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor1_coeff1") {
        calib.sensor1[1] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor2_coeff0") {
        calib.sensor2[0] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor2_coeff1") {
        calib.sensor2[1] = server.arg(i).toFloat();
      }
      else if (server.argName(i) == "sensor2_coeff2") {
        calib.sensor2[2] = server.arg(i).toFloat();
      }
    }
    calibration::save_file();
}
