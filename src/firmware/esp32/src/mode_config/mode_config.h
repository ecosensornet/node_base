#pragma once

#include <html_server.h>

#include "../settings.h"

extern HtmlServer server;
extern String meas_cur;

namespace mode_config {

void setup();
void loop();

}  // namespace mode_config
