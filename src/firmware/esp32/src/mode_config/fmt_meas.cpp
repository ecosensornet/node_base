#include <ArduinoJson.h>

#include "../config/config.h"
#include "../measure/measure.h"

#include "fmt_meas.h"


void mode_config::fmt_meas(String& msg) {
  StaticJsonDocument<128> doc;

  doc["sensor1"] = sample_cur.sensor1;
  doc["sensor1_cal"] = sample_cur.sensor1_cal;
  doc["sensor2"] = sample_cur.sensor2;
  doc["sensor2_cal"] = sample_cur.sensor2_cal;

  serializeJsonPretty(doc, msg);
}

