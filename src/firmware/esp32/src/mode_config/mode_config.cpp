#include <WiFi.h>
#include <WiFiClient.h>
#include <LittleFS.h>

#include "html_calibration.h"
#include "html_mode_espnow.h"
#include "html_mode_lora.h"
#include "html_root.h"
#include "html_sampling.h"
#include "mode_config.h"

const char *ssid = "ecosensornet";
const char *password = "floatingworld";

HtmlServer server(80);

void mode_config::setup() {
#if (DEBUG == 1)
  Serial.println("setup config");
#endif
  //   LittleFS.begin(); has been done in node_cfg already

  WiFi.softAP(ssid, password);
#if (DEBUG == 1)
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
#endif

  server.on("/", html_root::handle_html);
  server.on("/style.css", html_root::handle_style);
  server.on("/favicon.ico", html_root::handle_favicon);
  server.on("/page_calibration.html", html_calibration::handle_html);
  server.on("/chg_mode_espnow", html_mode_espnow::handle_chg_mode);
  server.on("/chg_mode_lora", html_mode_lora::handle_chg_mode);
  server.on("/page_mode_espnow.html", html_mode_espnow::handle_html);
  server.on("/page_mode_lora.html", html_mode_lora::handle_html);
  server.on("/page_sampling.html", html_sampling::handle_html);
  server.on_not_found(html_root::handle_not_found);

  server.begin();
#if (DEBUG == 1)
  Serial.println("HTTP server started");
#endif
}


void mode_config::loop() {
  server.handleClient();
}

