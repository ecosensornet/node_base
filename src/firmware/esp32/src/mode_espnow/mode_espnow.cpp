#include <WiFi.h>

#include "../settings.h"
#include "../config/config.h"
#include "../measure/measure.h"

#include "fmt_meas.h"
#include "mode_espnow.h"

uint8_t all_cnt;
uint8_t encryp_cnt;
esp_now_peer_info_t gateway_info;
bool msg_send;


void mode_espnow::on_data_sent(const uint8_t* mac_addr, esp_now_send_status_t status) {
    msg_send = true;
#if (DEBUG == 1)
  Serial.print("Last Packet Send Status: ");
  if (status == ESP_NOW_SEND_SUCCESS) {
    Serial.println("Delivery success");
  }
  else {
    Serial.print("Delivery fail (");
    Serial.print(status);
    Serial.print(") to: ");
    for (uint8_t i = 0; i < 6; i++) {
      Serial.print(*mac_addr, HEX);
      mac_addr ++;
      Serial.print(", ");
    }
    Serial.println("");
  }
#endif // DEBUG
}


void mode_espnow::setup() {
#if (DEBUG == 1)
  Serial.println("setup local espnow");
#endif // DEBUG
  WiFi.mode(WIFI_STA);
  if (esp_now_init() != ESP_OK) {
#if (DEBUG == 1)
    Serial.println("Error initializing ESP-NOW");
#endif // DEBUG
    return;
  }

  esp_now_register_send_cb(mode_espnow::on_data_sent);

  // register gateway
  memcpy(gateway_info.peer_addr, cfg.gate_ip, 6);
  gateway_info.channel = 0;
  gateway_info.encrypt = false;

  if (esp_now_add_peer(&gateway_info) != ESP_OK){
#if (DEBUG == 1)
    uint8_t* mac_addr = cfg.gate_ip;
    Serial.print("Failed to register peer: ");
    for (uint8_t i = 0; i < 6; i++) {
      Serial.print(*mac_addr, HEX);
      mac_addr ++;
      Serial.print(", ");
    }
    Serial.println("");
#endif // DEBUG
    pinMode(LED_BUILTIN, OUTPUT);
    for (uint8_t i = 0; i < 10; i++) {
      digitalWrite(LED_BUILTIN, HIGH);
      delay(100);
      digitalWrite(LED_BUILTIN, LOW);
      delay(200);
    }
    ESP.deepSleep(0);
  }
}


void mode_espnow::loop() {
  long sleep_time;
#if (DEBUG == 1)
  Serial.println("loop local espnow");
#endif // DEBUG

  // perform measure
  measure::sample();

  // send message
  uint8_t msg[256];
  uint8_t msg_len = mode_espnow::fmt_meas(msg);

  msg_send = false;
  esp_now_send(cfg.gate_ip, msg, msg_len);

  while (!msg_send) {
#if (DEBUG == 1)
    Serial.println("waiting send");
#endif // DEBUG
    delay(20);
  }
#if (DEBUG == 1)
  Serial.print("delay: ");
  Serial.print(cfg.interval);
  Serial.println(" [s]");
#endif // DEBUG

  sleep_time = cfg.interval * 1e3 - millis();
#if (DEBUG == 1)
  sleep_time -= 300;
  Serial.print("sleep_time: ");
  Serial.print(sleep_time);
  Serial.println(" [ms]");
  delay(300);  // needed for serial to send msg
#endif // DEBUG
  if (sleep_time > 0) {
    ESP.deepSleep(sleep_time * 1e3);
  }
  else {
#if (DEBUG == 1)
    Serial.println("restart");
    delay(300);  // needed for serial to send msg
#endif // DEBUG
    ESP.deepSleep(0);
  }
}
