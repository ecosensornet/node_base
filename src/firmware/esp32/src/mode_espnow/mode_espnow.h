#pragma once

#include <Arduino.h>

#include <esp_now.h>

namespace mode_espnow {

void on_data_sent(const uint8_t*, esp_now_send_status_t);
void setup();
void loop();

}  // namespace mode_espnow
