#include "../settings.h"
#include "../measure/measure.h"

#include "fmt_meas.h"


uint8_t mode_espnow::fmt_meas(uint8_t* msg) {
  uint8_t i;

  // interface id
  msg[0] = 0;

  // sensor1
  cvtd.val = sample_cur.sensor1;
  for (i=0;i<4;i++){
    msg[1+i] = cvtd.bytes[i];
  }
  cvtf.val = sample_cur.sensor1_cal;
  for (i=0;i<4;i++){
    msg[5+i] = cvtf.bytes[i];
  }

  // sensor2
  cvtd.val = sample_cur.sensor2;
  for (i=0;i<4;i++){
    msg[9+i] = cvtd.bytes[i];
  }
  cvtf.val = sample_cur.sensor2_cal;
  for (i=0;i<4;i++){
    msg[13+i] = cvtf.bytes[i];
  }

#if (DEBUG == 1)
  Serial.print("cur: (");
  Serial.print(sample_cur.sensor1);
  Serial.print(", ");
  Serial.print(sample_cur.sensor1_cal);
  Serial.print(", ");
  Serial.print(sample_cur.sensor2);
  Serial.print(", ");
  Serial.print(sample_cur.sensor2_cal);
  Serial.println(")");

  Serial.print("[");
  for (uint8_t i = 0; i < 17; i++) {
    Serial.print(msg[i]);
    Serial.print(", ");
  }
  Serial.println("]");
#endif // DEBUG

  return 17;
}

