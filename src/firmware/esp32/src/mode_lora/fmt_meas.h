#pragma once

#include <Arduino.h>

namespace mode_lora {

uint8_t fmt_meas(uint8_t*);

}  // namespace mode_lora
