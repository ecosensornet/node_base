#include "src/board.h"
#include "src/node.h"
#include "src/settings.h"


void setup() {
  board::setup();
  node::setup();
}

void loop() {
  board::signal_normal();
  node::loop();
}
