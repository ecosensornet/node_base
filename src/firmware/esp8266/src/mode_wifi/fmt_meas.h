#ifndef MODE_WIFI_FMT_MEASURE_H
#define MODE_WIFI_FMT_MEASURE_H

#include "../settings.h"

namespace mode_wifi {

void fmt_meas(String& msg);

}  // namespace mode_wifi

#endif //  MODE_WIFI_FMT_MEASURE_H
