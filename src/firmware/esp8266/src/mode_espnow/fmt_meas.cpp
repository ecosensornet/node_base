#include <ArduinoJson.h>

#include "../config/node_cfg.h"
#include "../measure/node_measure.h"

#include "fmt_meas.h"


void mode_espnow::fmt_meas(String& msg) {
  StaticJsonDocument<192> doc;

  doc["nid"] = node_id;
  doc["iid"] = 2;
  doc["interval"] = cfg.interval;
  doc["nb"] = 1;

  JsonArray events = doc.createNestedArray("events");

  JsonObject events_0 = events.createNestedObject();
  events_0["sensor1"] = sample_cur.sensor1;
  events_0["sensor1_cal"] = sample_cur.sensor1_cal;
  events_0["sensor2"] = sample_cur.sensor2;
  events_0["sensor2_cal"] = sample_cur.sensor2_cal;

  serializeJson(doc, msg);
}

