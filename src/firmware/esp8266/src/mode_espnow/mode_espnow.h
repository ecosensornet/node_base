#ifndef MODE_ESPNOW_H
#define MODE_ESPNOW_H

#include "../settings.h"

namespace mode_espnow {

void on_data_sent(uint8_t*, uint8_t);
void setup();
void loop();

}  // namespace mode_espnow

#endif //  MODE_ESPNOW_H
