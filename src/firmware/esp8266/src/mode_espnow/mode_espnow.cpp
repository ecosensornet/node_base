#include <ESP8266WiFi.h>
#include <espnow.h>
#include <ArduinoJson.h>

#include "../config/node_cfg.h"
#include "../measure/node_measure.h"

#include "fmt_meas.h"
#include "mode_espnow.h"

uint8_t all_cnt;
uint8_t encryp_cnt;

void mode_espnow::on_data_sent(uint8_t* mac_addr, uint8_t sendStatus) {
#if (DEBUG == 1)
  Serial.print("Last Packet Send Status: ");
  if (sendStatus == 0) {
    Serial.println("Delivery success");
  }
  else {
    Serial.print("Delivery fail (");
    Serial.print(sendStatus, DEC);
    Serial.print(") to: ");
    for (uint8_t i = 0; i < 6; i++) {
      Serial.print(*mac_addr, HEX);
      mac_addr ++;
      Serial.print(", ");
    }
    Serial.println("");
  }
#endif // DEBUG
}


void mode_espnow::setup() {
#if (DEBUG == 1)
  Serial.println("setup local espnow");
#endif // DEBUG
  WiFi.mode(WIFI_STA);
  if (esp_now_init() != 0) {
#if (DEBUG == 1)
    Serial.println("Error initializing ESP-NOW");
#endif // DEBUG
    return;
  }

  esp_now_set_self_role(ESP_NOW_ROLE_CONTROLLER);
  esp_now_register_send_cb(mode_espnow::on_data_sent);

  // register gateway
  if (esp_now_add_peer(cfg.gate_ip, ESP_NOW_ROLE_SLAVE, 1, NULL, 0) != 0) {
#if (DEBUG == 1)
    uint8_t* mac_addr = cfg.gate_ip;
    Serial.print("Failed to register peer: ");
    for (uint8_t i = 0; i < 6; i++) {
      Serial.print(*mac_addr, HEX);
      mac_addr ++;
      Serial.print(", ");
    }
    Serial.println("");
#endif // DEBUG
    pinMode(LED_BUILTIN, OUTPUT);
    for (uint8_t i = 0; i < 10; i++) {
      digitalWrite(LED_BUILTIN, HIGH);
      delay(100);
      digitalWrite(LED_BUILTIN, LOW);
      delay(200);
    }
    ESP.restart();
  }
}


void mode_espnow::loop() {
  unsigned long elapsed = millis();
  String msg = "";
#if (DEBUG == 1)
  Serial.println("loop local espnow");
#endif // DEBUG

  // perform measure
  measure::sample();

  // send message
  mode_espnow::fmt_meas(msg);
#if (DEBUG == 1)
  Serial.println(msg);
#endif // DEBUG

  esp_now_send(cfg.gate_ip, (uint8_t*) msg.c_str(), msg.length() + 1);

  //elapsed = millis() - elapsed;  // may fail when millis overflow (once every 50 days as been deemed acceptable)
  elapsed = millis();
  if (elapsed < cfg.interval * 1e3) {
    // delay(cfg.interval * 1e3 - elapsed);
    ESP.deepSleep((cfg.interval * 1e3 - elapsed) * 1e3);
  }

}
