#ifndef MODE_ESPNOW_FMT_MEASURE_H
#define MODE_ESPNOW_FMT_MEASURE_H

#include "../settings.h"

namespace mode_espnow {

void fmt_meas(String& msg);

}  // namespace mode_espnow

#endif //  MODE_ESPNOW_FMT_MEASURE_H
