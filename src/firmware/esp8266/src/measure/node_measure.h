#ifndef NODE_MEASURE_H
#define NODE_MEASURE_H

#include "../settings.h"

namespace measure {

typedef struct {
  uint32_t sensor1;
  float sensor1_cal;
  uint32_t sensor2;
  float sensor2_cal;
} Sample;

void setup();
void sample();

void fmt_config(String& msg);

}  // namespace measure

extern measure::Sample sample_cur;

#endif //  NODE_MEASURE_H
