#ifndef NODE_CFG_H
#define NODE_CFG_H

#include "../settings.h"

#define MODE_CONFIG 0
#define MODE_ESPNOW 1
#define MODE_WIFI 2
#define MODE_LORA 3

typedef struct {
  uint32_t crc32;  // used to ensure data are correct
  uint8_t mode;
  uint16_t interval;  // [s] time between two measures

  // local espnow settings
  uint8_t gate_ip[6];  // MAC address of gateway

  // local wifi settings
  uint8_t lsip[4];  // IP address of server
} NodeConfig;

extern NodeConfig cfg;
extern char node_id[13];

uint32_t eval_crc();

void setup_cfg();
void load_cfg_rtcmem();
void save_cfg_rtcmem();

void load_cfg_file();
void save_cfg_file();

void fetch_node_id(char*);

#endif //  NODE_CFG_H
