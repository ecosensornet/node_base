#ifndef MODE_LORA_FMT_MEASURE_H
#define MODE_LORA_FMT_MEASURE_H

#include "../settings.h"

namespace mode_lora {

void fmt_meas(String& msg);

}  // namespace mode_lora

#endif //  MODE_LORA_FMT_MEASURE_H
