#ifndef MODE_LORA_H
#define MODE_LORA_H

#include "../settings.h"

namespace mode_lora {

void setup();
void loop();

}  // namespace mode_lora

#endif //  MODE_LORA_H
