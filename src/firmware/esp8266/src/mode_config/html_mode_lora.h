#ifndef MODE_LORA_HTML_H
#define MODE_LORA_HTML_H

#include "../settings.h"

namespace mode_lora {

String processor(const String& key);
void handle_html();

}  // namespace mode_lora

#endif //  MODE_LORA_HTML_H
