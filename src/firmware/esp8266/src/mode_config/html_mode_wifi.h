#ifndef MODE_WIFI_HTML_H
#define MODE_WIFI_HTML_H

#include "../settings.h"

namespace mode_wifi {

String processor(const String& key);
void handle_html();
void handle_chg_mode();

}  // namespace mode_wifi

#endif //  MODE_WIFI_HTML_H
