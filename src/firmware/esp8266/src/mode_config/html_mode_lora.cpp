#include "../config/node_cfg.h"
#include "../mode_config/mode_config.h"

#include "html_mode_lora.h"


String mode_lora::processor(const String& key) {
  return "Key not found";
}


void mode_lora::handle_html() {
#if (DEBUG == 1)
  Serial.println("serving mode_lora.html");
#endif
  server.process_and_send("/mode_lora.html", mode_lora::processor);
}

