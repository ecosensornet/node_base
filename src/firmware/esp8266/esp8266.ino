#include "src/node.h"

void setup() {
#if (DEBUG == 1)
  Serial.begin(115200);
  while (!Serial) {
    delay(10);
  }
  delay(300);
#endif // DEBUG

  node::setup();
}

void loop() {
  node::loop();
}
