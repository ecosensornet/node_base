"""
Decode message into values
"""
import json
import struct
from dataclasses import dataclass


@dataclass
class Sample:
    iid: int = 0
    """Interface id
    """

    sensor1: int = 0
    """sensor1
    """

    sensor1_cal: float = 0
    """sensor1 calibrated
    """

    sensor2: int = 0
    """sensor1
    """

    sensor2_cal: float = 0
    """sensor1 calibrated
    """

    def decode(self, buffer):
        if buffer[0] != self.iid:
            raise IndexError("bad interface")

        self.sensor1, self.sensor1_cal, self.sensor2, self.sensor2_cal = struct.unpack('IfIf', buffer[1:])

    def to_json(self):
        data = {
            "sensor1": self.sensor1,
            "sensor1_cal": self.sensor1_cal,
            "sensor2": self.sensor2,
            "sensor2_cal": self.sensor2_cal,
        }

        return json.dumps(data, indent=2)
